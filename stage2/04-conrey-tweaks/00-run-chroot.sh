systemctl enable sshd && systemctl start sshd

[ -d /usr/share/plymouth/themes/userfriendly ] || git clone https://gitlab.com/alexconrey/plymouth-userfriendly /usr/share/plymouth/themes/userfriendly

plymouth-set-default-theme -R userfriendly

## This is kind of dumb because its checking the available space in the chroot environment but whatever i guess
# seemed like a good idea at the time and i dont want to delete commands that i spent 2 minutes on /shrug
free_mb=$(( $(df / | awk {'$4 == "Avail"; print $4'} | grep -v 'Avail')/(1024) ))

# If there's more than 4GB free, create 1GB swapfile
if [ $free_mb -gt 4096 ]; then
    dd if=/dev/zero of=/var/tmp/swapfile bs=1 count=0 seek=1G
    [ -f /var/tmp/swapfile ] || (echo 'Could not create swapfile' && exit 1)
    mkswap /var/tmp/swapfile
fi
